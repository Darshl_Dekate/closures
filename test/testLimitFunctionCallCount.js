const limitFunctionCallCount = require('../limitFunctionCallCount.js');


const sumTwo = (a,b) => a + b ;

const nTimes = limitFunctionCallCount(sumTwo,2);

console.log(nTimes(1,2))
console.log(nTimes(3,4))
