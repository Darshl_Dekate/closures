
function limitFunctionCallCount(sumTwo, n) {
  
  let counter = n;
 
  function invoke(...destruct){

    if (counter > 0){
        ++counter;
        return sumTwo(...destruct);
    }
    else{
        return null;
    }
}
return invoke;


}


module.exports = limitFunctionCallCount;