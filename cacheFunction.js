function cacheFunction(cb) {

  const cache = new Set();

  function invoke(arg) {
    if (!cache.has(arg)) {

      cache.add(arg);
      return cb(arg);
    }
    else {

      return cache;
    }
  }
  return { invoke };
}
module.exports = cacheFunction;


